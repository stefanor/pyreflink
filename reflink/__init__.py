# -*- coding: utf-8 -*-

"""Top-level package for Python reflink."""

__author__ = """Ruben De Smet"""
__email__ = 'pypi@rubdos.be'
__version__ = '0.1.3'

from .error import ReflinkImpossibleError
from .reflink import reflink
from .reflink import supported_at
