if "%APPVEYOR_BUILD_WORKER_IMAGE%"=="Visual Studio 2017" (
	if "%PYTHON%"=="C:\Python36-x64" (
		build.cmd %PYTHON%\\python.exe setup.py bdist_wheel upload
	) else (
        if "%PYTHON%"=="C:\Python36" (
            build.cmd %PYTHON%\\python.exe setup.py bdist_wheel upload
        ) else (
            echo "Not a Python 3.6 build, not deploying"
        )
    )
) else (
    echo "Not deploying non Visual Studio 2017 builds"
)
